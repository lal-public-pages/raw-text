### SOLID Principle
This is an acronym for the first five object-oriented design (OOD) principles by Robert C. Martin.

- Single Responsiblity Principle
    - A class should have one and only one reason to change, meaning that a class should have only one job.
- Open Closed Principle
    - Objects or entities should be open for extension, but closed for modification.
- Liskov Substitution Principle (Barbara Liskov 1988)
    - Let q(x) be a property provable about objects of x of type T. Then q(y) should be provable for objects y of type S where S is a subtype of T.
- Interface Segregation Principle
    - A client should never be forced to implement an interface that it doesn't use or clients shouldn't be forced to depend on methods they do not use.
- Dependency Inversion Principle
    - Entities must depend on abstractions not on concretions. It states that the high level module must not depend on the low level module, but they should depend on abstractions.

----

### CAP Theorem
This is for distributed computing was published by Eric Brewer. This states that it is not possible for a distributed computer system to simultaneously provide all three of the following guarantees:

- Consistency
    - All nodes see the same data even at the same time with concurrent updates
- Availability
    - A guarantee that every request receives a response about whether it was successful or failed
- Partition Tolerance
    - The system continues to operate despite arbitrary message loss or failure of part of the system

----

### ACID Compliance

- Atomicity
    - A transaction must be completed in its entirety or not at all. If a transaction aborts in the middle, all operations up to that point must be completely nullified.
- Consistency
    - A transaction must transform a database from one consistent state to another consistent state. In other words, all data in a database must work as a state machine. The database must ensure all data is consistent at all times with all rules.
- Isolation
    - Each transaction must occur independently of other transactions occurring at the same time. In other words, queries and transactions always run at a point in time. You can query data while many other users are changing data and you will not see their changes, and they will not see each other’s changes.
- Durability
    - Committed transactions must be fully recoverable in all but the most extreme circumstances. Write-ahead logs provide absolute data durability until data is eventually written into permanent data and index files.

----

### OOPS Concepts

- Encapsulation
- Polymorphism
    - Overloading (Static or Compile Time)
    - Overriding (Dynamic or Runtime)
- Inheritance
- Abstraction

----

### SDLC Phases

1. Requirement collection and analysis
2. Feasibility study
3. Design
4. Coding
5. Testing
6. Installation/Deployment
7. Maintenance

----

### Feasibilities Checks

- Economic
    - Can we complete the project within the budget?
- Legal
    - Can we handle this project as cyber law and other regulatory framework/compliances?
- Operational
    - Can we create operations which is expected by the client?
- Technical
    - Can the client computer system support the software?
- Schedule
    - Can the project can be completed within the given schedule?

----

### MoSCoW Prioritization

- Must have
    - Cannot deliver a product without it.
- Should have
    - Important but not a critical one. Need to implement as soon as possible.
- Could have
    - Wanted or desirable but less important changes.
- Won't have (this time)
    - Least important changes. These can be added in later releases after all other priorities.

----

### Misc Principles

- KISS Principle (Keep it simple son/stupid/sucker/shit head)
    - Keep your code simple and clear to understand so that it's easy for anyone to make changes and fix bugs.
- YAGNI Principle (You Aren't Gonna Need It)
    - Don't waste time by adding features to the project you currently don't need.
- DRY Principle (Don't Repeat Yourself)
    - While writing your code avoid copy-pasting your code in different places. Otherwise, future maintenance will be difficult. Try to write modular code so that those can be reused.
- Occam's Razor
    - It states that in a group of hypotheses, always select the one that has the fewest assumptions. (William Occam 14th Century philosopher)
- CQRS (Command Query Responsibility Segregation)
    - CQRS separates reads and writes into different groups, using commands to update data, and queries to read data.

----

### Development Approaches

- Acceptance Test Driven Development (ATDD)
    - It is a modified form of TDD. The acceptance criteria are defined in early in application development process and then those criteria can be used to guide the subsequent development work. It helps to ensure that all project members understand precisely what needs to be done and implemented.
- Behaviour Driven Development (BDD)
    - It is similar in many ways to TDD except that the word "test" is replaced with the word "spec". It focuses on the behaviors of your system exhibits than the implementation details of it.
- Domain Driven Design (DDD)
    - It is an approach to software development that centers the development on programming a domain model that has a rich understanding of the processes and rules of a domain. It is about mapping business domain concepts into software artifacts.
- Feature Driven Development (FDD)
     - It organizes software development around making progress on features. Features in the FDD context, though, are not necessarily product features in the commonly understood sense. They are, rather, more akin to user stories in Scrum. In other words, "complete the login process" might be considered a feature.
- Test Driven Development (TDD)
    - It is a process of writing tests before writing code and make the test fail. Now write enough code so that test can pass and then at last refactor the code as per you requirement and make sure the test is passing again after the change.

----
