AM - Account Manager

AP - Accounts Payable

AR - Accounts Receivable

ATDD - Acceptance Test Driven Development

B2B - Business to Business

B2C - Business to Consumer

B2G - Business to Government

BDD - Behaviour Driven Development

CAO - Chief Administrative Officer or Chief Accounting Officer

CEO - Chief Executive Officer

CFO - Chief Financial Officer

CIO - Chief Investment Officer or Chief Information Officer

CISO - Chief Information Security Officer

CLO - Chief Legal Officer

CMO - Chief Marketing Officer

CMS - Content Management System

COB - Close of Business

COGS - Cost of Goods Sold

COO - Chief Operating Officer

CPA - Certified Public Accountant

CRM - Customer Relationship Management

CSO - Chief Security Officer

CQRS - Command and Query Responsibility Segregation

C2B - Consumer to Business

DDD - Domain Driven Design

EM - Engineering Manager

EOD - End of Day or End of Discussion

ETA - Estimated Time of Arrival

FDD - Feature Driven Development

FIFO - First In, First Out

FTE - Full-Time Employee

GP - Gross Profit

HQ - Headquarters

IPO - Initial Public Offering

ISP - Internet Service Provider

IRS - Internal Revenue Service

IRR - Internal Rate of Return

HR - Human Resources

KPI - Key Performance Indicator

NDA - Non-Disclosure Agreement

OOO - Out of Office

ORM - Object Relational Mapper

OS - Operating System

PE - Private Equity

PM - Project Manager

POS - Point of Sale

POC - Point of Contact

POCO - Plain old C object

POJO - Plain old java object

PTO - Paid Time Off

QA - Quality Assurance

Q1, Q2, Q3, Q4 - Quarters of the accounting year, fiscal year or calendar year

QC - Quality Control or Quality Costs

Re - Referring to

ROE - Return on Equity

ROI - Return on Investment

TDD - Test Driven Development

TOS - Terms of Service

VPN - Virtual Private Network

WFH - Work from home

YTD - Year to date
